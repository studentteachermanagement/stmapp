******STMApp - Student Management System******


STMApp is a Student Management System implemented with Spring Boot.

**How to Run**
- Make sure you have Maven and Java installed.

- To run the application locally, clone the repository, navigate to the folder, and execute the following commands:

**mvn clean install**

**mvn spring-boot:run**


The application will be available at http://localhost:8085

Endpoints
-
Explore the API using Swagger UI. After running the application, open http://localhost:8085/swagger-ui.html in your browser to access the interactive API documentation.

**API Test examples in doc folder**

Dependencies
- 
Spring Boot 3.2.1

Spring Boot Starter Data JPA

Spring Boot Starter Web

H2 Database

Spring Boot Starter Test

Project Lombok

Springdoc OpenAPI Starter WebMVC UI 2.0.2
