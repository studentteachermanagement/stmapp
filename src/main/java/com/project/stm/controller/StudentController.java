package com.project.stm.controller;

import com.project.stm.model.Student;
import com.project.stm.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/students")
@AllArgsConstructor
public class StudentController {

    @NonNull
    private final StudentService studentService;

    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable Long id) {
        try {
            return studentService.getStudentById(id)
                    .map(student -> new ResponseEntity<>(student, HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (Exception e) {
            log.error("Error while retrieving student by id: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping
    public ResponseEntity<Student> createStudent(@RequestBody Student student) {
        try {
            Optional<Student> createdStudent = studentService.createStudent(student);
            return createdStudent.map(s -> new ResponseEntity<>(s, HttpStatus.CREATED))
                    .orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
        } catch (Exception e) {
            log.error("Error while creating student", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody Student updatedStudent) {
        try {
            Optional<Student> updated = studentService.updateStudent(id, updatedStudent);
            return updated.map(student -> new ResponseEntity<>(student, HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (Exception e) {
            log.error("Error while updating student with id: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Student> patchStudent(@PathVariable Long id, @RequestBody Map<String, Object> fieldsToUpdate) {
        try {
            Optional<Student> existingStudent = studentService.getStudentById(id);

            if (existingStudent.isPresent()) {
                Student patchedStudent = studentService.patchStudent(id, fieldsToUpdate);
                return new ResponseEntity<>(patchedStudent, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error while patching student with id: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteStudent(@PathVariable Long id) {
        try {
            boolean deleted = studentService.deleteStudent(id);
            return deleted ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Error while deleting student with id: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/count")
    public ResponseEntity<Long> getStudentCount() {
        try {
            Long count = studentService.getStudentCount();
            return new ResponseEntity<>(count, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error while retrieving student count", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/search")
    public ResponseEntity<List<Student>> searchStudents(
            @RequestParam(name = "minAge", required = false) Long minAge,
            @RequestParam(name = "maxAge", required = false) Long maxAge,
            @RequestParam(name = "courseId", required = false) Long courseId,
            @RequestParam(name = "groupId", required = false) Long groupId
    ) {
        try {
            List<Student> students = studentService.searchStudents(minAge, maxAge, courseId, groupId);
            return new ResponseEntity<>(students, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error while searching students", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
