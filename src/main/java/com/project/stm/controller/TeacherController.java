package com.project.stm.controller;

import com.project.stm.model.Teacher;
import com.project.stm.service.TeacherService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/teachers")
@AllArgsConstructor
@Slf4j
public class TeacherController {

    @NonNull
    private final TeacherService teacherService;


    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getTeacherById(@PathVariable Long id) {
        try {
            return teacherService.getTeacherById(id)
                    .map(teacher -> new ResponseEntity<>(teacher, HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (Exception e) {
            log.error("Error while retrieving teacher by id: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<Teacher> createTeacher(@RequestBody Teacher teacher) {
        try {
            return new ResponseEntity<>(teacherService.createTeacher(teacher), HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Error while creating teacher", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PatchMapping("/{id}")
    public ResponseEntity<Teacher> patchTeacher(@PathVariable Long id, @RequestBody Map<String, Object> fieldsToUpdate) {
        try {
            Optional<Teacher> existingTeacher = teacherService.getTeacherById(id);

            if (existingTeacher.isPresent()) {
                Teacher patchedTeacher = teacherService.patchTeacher(id, fieldsToUpdate);
                return new ResponseEntity<>(patchedTeacher, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error while patching teacher with id: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/{id}")
    public ResponseEntity<Teacher> updateTeacher(@PathVariable Long id, @RequestBody Teacher updatedTeacher) {
        try {
            return teacherService.updateTeacher(id, updatedTeacher)
                    .map(teacher -> new ResponseEntity<>(teacher, HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (Exception e) {
            log.error("Error while updating teacher with id: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTeacher(@PathVariable Long id) {
        try {
            Optional<Teacher> existingTeacher = teacherService.getTeacherById(id);

            if (existingTeacher.isPresent()) {
                teacherService.deleteTeacher(id);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error while deleting teacher with id: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/count")
    public ResponseEntity<Long> getTeacherCount() {
        try {
            return new ResponseEntity<>(teacherService.getTeacherCount(), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error while retrieving teacher count", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/search")
    public ResponseEntity<List<Teacher>> searchTeachers(
            @RequestParam(name = "groupId", required = false) Long groupId,
            @RequestParam(name = "courseId", required = false) Long courseId,
            @RequestParam(name = "minAge", required = false) Long minAge,
            @RequestParam(name = "maxAge", required = false) Long maxAge) {
        try {
            List<Teacher> teachers = teacherService.searchTeachers(groupId, courseId, minAge, maxAge);
            return new ResponseEntity<>(teachers, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error while searching teachers", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
