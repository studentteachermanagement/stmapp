package com.project.stm.model;

public enum CourseType {
    MAIN,
    SECONDARY
}