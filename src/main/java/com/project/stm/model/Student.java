package com.project.stm.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "students")
@Data
@ToString(exclude = {"group", "courses"})
@Builder(toBuilder = true)
@EqualsAndHashCode(exclude = {"group", "courses"}, callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class Student extends Person {
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "student_courses",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id")
    )
    private List<Course> courses;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;
}
