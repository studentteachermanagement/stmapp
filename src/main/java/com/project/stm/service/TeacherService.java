package com.project.stm.service;

import com.project.stm.model.Course;
import com.project.stm.model.Group;
import com.project.stm.model.Teacher;
import com.project.stm.repository.TeacherRepository;
import jakarta.persistence.criteria.Join;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TeacherService {

    private final TeacherRepository teacherRepository;

    @Autowired
    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public List<Teacher> getAllTeachers() {
        return teacherRepository.findAll();
    }

    public Optional<Teacher> getTeacherById(Long id) {
        return teacherRepository.findById(id);
    }

    public Teacher createTeacher(Teacher teacher) {
        if (isInvalidTeacher(teacher)) {
            throw new IllegalArgumentException("Teacher data is invalid");
        }
        return teacherRepository.save(teacher);
    }

    public Optional<Teacher> updateTeacher(Long id, Teacher updatedTeacher) {
        if (isInvalidTeacher(updatedTeacher)) {
            throw new IllegalArgumentException("Updated teacher data is invalid");
        }
        return teacherRepository.findById(id)
                .map(existingTeacher -> {
                    existingTeacher.setFirstName(updatedTeacher.getFirstName());
                    existingTeacher.setLastName(updatedTeacher.getLastName());
                    existingTeacher.setCourses(updatedTeacher.getCourses());
                    existingTeacher.setGroups(updatedTeacher.getGroups());
                    return teacherRepository.save(existingTeacher);
                });
    }

    public Teacher patchTeacher(Long id, Map<String, Object> fieldsToUpdate) {
        Optional<Teacher> existingTeacher = teacherRepository.findById(id);

        if (existingTeacher.isPresent()) {
            Teacher teacher = existingTeacher.get();
            applyPartialUpdate(teacher, fieldsToUpdate);
            return teacherRepository.save(teacher);
        } else {
            throw new IllegalArgumentException("Teacher not found with id: " + id);
        }
    }

    private void applyPartialUpdate(Teacher target, Map<String, Object> updates) {
        BeanWrapper targetWrapper = new BeanWrapperImpl(target);

        updates.forEach((key, value) -> {
            if (value != null) {
                targetWrapper.setPropertyValue(key, value);
            }
        });
    }

    public void deleteTeacher(Long id) {
        teacherRepository.deleteById(id);
    }

    public Long getTeacherCount() {
        return teacherRepository.count();
    }
    public boolean isInvalidTeacher(Teacher teacher) {
        return teacher == null ||
                teacher.getFirstName() == null || teacher.getFirstName().isEmpty() ||
                teacher.getLastName() == null || teacher.getLastName().isEmpty() ||
                teacher.getAge() == null;
    }

    public List<Teacher> searchTeachers(Long groupId, Long courseId, Long minAge, Long maxAge) {
        Specification<Teacher> spec = Specification.where(null);

        if (groupId != null) {
            spec = spec.and((root, query, builder) -> {
                Join<Teacher, Group> groupJoin = root.join("groups");
                return builder.equal(groupJoin.get("id"), groupId);
            });
        }
        if (courseId != null) {
            spec = spec.and((root, query, builder) -> {
                Join<Teacher, Course> courseJoin = root.join("courses");
                return builder.equal(courseJoin.get("id"), courseId);
            });
        }

        if (minAge != null) {
            spec = spec.and((root, query, builder) ->
                    builder.greaterThanOrEqualTo(root.get("age"), minAge));
        }

        if (maxAge != null) {
            spec = spec.and((root, query, builder) ->
                    builder.lessThanOrEqualTo(root.get("age"), maxAge));
        }

        return minAge == null && maxAge == null && courseId == null && groupId == null ?
            teacherRepository.findAll() : teacherRepository.findAll(spec);
    }
}
