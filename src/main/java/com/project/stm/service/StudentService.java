package com.project.stm.service;

import com.project.stm.model.Course;
import com.project.stm.model.Group;
import com.project.stm.model.Student;
import com.project.stm.repository.GroupRepository;
import com.project.stm.repository.StudentRepository;
import jakarta.persistence.criteria.Join;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;


    @Autowired
    public StudentService(StudentRepository studentRepository, GroupRepository groupRepository) {
        this.studentRepository = studentRepository;
        this.groupRepository = groupRepository;
    }

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    public Optional<Student> getStudentById(Long id) {
        return studentRepository.findById(id);
    }

    public Optional<Student> createStudent(Student student) {
        if (student.getGroup() != null && student.getGroup().getGroupId() != null) {
            Group group = groupRepository.findById(student.getGroup().getGroupId()).orElse(null);
            student.setGroup(group);
        }
        Student savedStudent = studentRepository.save(student);
        return Optional.of(savedStudent);
    }

    public Optional<Student> updateStudent(Long id, Student updatedStudent) {
        if (isInvalidStudent(updatedStudent)) {
            throw new IllegalArgumentException("Updated student data is invalid");
        }
        return studentRepository.findById(id)
                .map(existingStudent -> {
                    existingStudent.setFirstName(updatedStudent.getFirstName());
                    existingStudent.setLastName(updatedStudent.getLastName());
                    existingStudent.setAge(updatedStudent.getAge());
                    existingStudent.setCourses(updatedStudent.getCourses());
                    existingStudent.setGroup(updatedStudent.getGroup());
                    return studentRepository.save(existingStudent);
                });
    }

    public Student patchStudent(Long id, Map<String, Object> fieldsToUpdate) {
        Optional<Student> existingStudent = studentRepository.findById(id);

        if (existingStudent.isPresent()) {
            Student student = existingStudent.get();
            applyPartialUpdate(student, fieldsToUpdate);
            return studentRepository.save(student);
        } else {
            throw new IllegalArgumentException("Student not found with id: " + id);
        }
    }

    private void applyPartialUpdate(Student target, Map<String, Object> updates) {
        BeanWrapper targetWrapper = new BeanWrapperImpl(target);

        updates.forEach((key, value) -> {
            if (value != null) {
                targetWrapper.setPropertyValue(key, value);
            }
        });
    }

    public boolean deleteStudent(Long id) {
        if (studentRepository.existsById(id)) {
            studentRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Long getStudentCount() {
        return studentRepository.count();
    }
    private boolean isInvalidStudent(Student student) {
        return student == null ||
                student.getFirstName() == null || student.getFirstName().isEmpty() ||
                student.getLastName() == null || student.getLastName().isEmpty() ||
                student.getAge() == null;
    }

    public List<Student> searchStudents(Long minAge, Long maxAge, Long courseId, Long groupId) {
        Specification<Student> spec = Specification.where(null);

        if (groupId != null) {
            spec = spec.and((root, query, builder) ->
                    builder.equal(root.get("group").get("groupId"), groupId));
        }

        if (minAge != null) {
            spec = spec.and((root, query, builder) ->
                    builder.greaterThanOrEqualTo(root.get("age"), minAge));
        }

        if (maxAge != null) {
            spec = spec.and((root, query, builder) ->
                    builder.lessThanOrEqualTo(root.get("age"), maxAge));
        }


        if (courseId != null) {
            spec = spec.and((root, query, builder) -> {
                Join<Student, Course> courseJoin = root.join("courses");
                return builder.equal(courseJoin.get("id"), courseId);
            });
        }

        return minAge == null && maxAge == null && courseId == null && groupId == null ?
                studentRepository.findAll() : studentRepository.findAll(spec);
    }
}
