package com.project.stm.service;

import com.project.stm.model.Course;
import com.project.stm.model.CourseType;
import com.project.stm.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    private final CourseRepository courseRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    public Course getCourseById(Long id) {
        return courseRepository.findById(id).orElse(null);
    }

    public Course createCourse(Course course) {
        return courseRepository.save(course);
    }

    public void deleteCourse(Long id) {
        courseRepository.deleteById(id);
    }

    public long getNumberOfCoursesByType(CourseType courseType) {
        return courseRepository.findAll().stream()
                .filter(course -> courseType.equals(course.getType()))
                .count();
    }
}

