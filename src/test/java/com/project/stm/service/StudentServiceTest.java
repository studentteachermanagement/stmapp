package com.project.stm.service;

import com.project.stm.model.Group;
import com.project.stm.model.Student;
import com.project.stm.repository.GroupRepository;
import com.project.stm.repository.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


import static org.mockito.ArgumentMatchers.any;

class StudentServiceTest {

    @Mock
    private StudentRepository studentRepository;

    @Mock
    private GroupRepository groupRepository;

    @InjectMocks
    private StudentService studentService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllStudents_Success() {
        when(studentRepository.findAll()).thenReturn(Collections.emptyList());

        List<Student> students = studentService.getAllStudents();

        assertNotNull(students);
        assertEquals(0, students.size());
        verify(studentRepository, times(1)).findAll();
    }

    @Test
    void createStudent_WithValidData_Success() {
        Student studentToCreate = new Student();
        studentToCreate.setFirstName("John");
        studentToCreate.setLastName("Doe");
        studentToCreate.setAge(25L);

        when(studentRepository.save(any())).thenReturn(studentToCreate);
        when(groupRepository.findById(any())).thenReturn(Optional.of(new Group()));

        Optional<Student> result = studentService.createStudent(studentToCreate);

        assertTrue(result.isPresent());
        assertEquals(studentToCreate, result.get());
        verify(studentRepository, times(1)).save(any());
        verify(groupRepository, times(0)).findById(any());
    }

    @Test
    void getStudentById_ExistingId_Success() {
        Long existingId = 1L;
        Student existingStudent = new Student();
        existingStudent.setId(existingId);

        when(studentRepository.findById(existingId)).thenReturn(Optional.of(existingStudent));

        Optional<Student> result = studentService.getStudentById(existingId);

        assertTrue(result.isPresent());
        assertEquals(existingStudent, result.get());
        verify(studentRepository, times(1)).findById(existingId);
    }

    @Test
    void getStudentById_NonExistingId_ReturnsEmptyOptional() {
        Long nonExistingId = 99L;

        when(studentRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        Optional<Student> result = studentService.getStudentById(nonExistingId);

        assertTrue(result.isEmpty());
        verify(studentRepository, times(1)).findById(nonExistingId);
    }

    @Test
    void updateStudent_WithValidData_Success() {
        Long existingId = 1L;
        Student existingStudent = new Student();
        existingStudent.setId(existingId);
        existingStudent.setFirstName("John");
        existingStudent.setLastName("Doe");
        existingStudent.setAge(25L);

        Student updatedStudent = new Student();
        updatedStudent.setFirstName("Updated");
        updatedStudent.setLastName("Student");
        updatedStudent.setAge(30L);

        when(studentRepository.findById(existingId)).thenReturn(Optional.of(existingStudent));
        when(studentRepository.save(any())).thenReturn(updatedStudent);

        Optional<Student> result = studentService.updateStudent(existingId, updatedStudent);

        assertTrue(result.isPresent());
        assertEquals(updatedStudent, result.get());
        verify(studentRepository, times(1)).findById(existingId);
        verify(studentRepository, times(1)).save(any());
    }

    @Test
    void deleteStudent_ExistingId_Success() {
        Long existingId = 1L;
        when(studentRepository.existsById(existingId)).thenReturn(true);

        boolean result = studentService.deleteStudent(existingId);

        assertTrue(result);
        verify(studentRepository, times(1)).existsById(existingId);
        verify(studentRepository, times(1)).deleteById(existingId);
    }

    @Test
    void getStudentCount_Success() {
        when(studentRepository.count()).thenReturn(5L);

        Long count = studentService.getStudentCount();

        assertEquals(5L, count);
        verify(studentRepository, times(1)).count();
    }
    @Test
    void patchStudent_WithValidData_Success() {
        Long existingId = 1L;
        Student existingStudent = new Student();
        existingStudent.setId(existingId);
        existingStudent.setFirstName("John");
        existingStudent.setLastName("Doe");
        existingStudent.setAge(25L);

        Map<String, Object> updates = Map.of("firstName", "Updated", "age", 30L);

        when(studentRepository.findById(existingId)).thenReturn(Optional.of(existingStudent));
        when(studentRepository.save(any())).thenReturn(existingStudent);

        Student result = studentService.patchStudent(existingId, updates);

        assertNotNull(result);
        assertEquals("Updated", result.getFirstName());
        assertEquals(30L, result.getAge());
        verify(studentRepository, times(1)).findById(existingId);
        verify(studentRepository, times(1)).save(any());
    }

    @Test
    void patchStudent_WithInvalidId_ThrowsException() {
        Long nonExistingId = 99L;
        Map<String, Object> updates = Map.of("firstName", "Updated", "age", 30L);

        when(studentRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> studentService.patchStudent(nonExistingId, updates));
        verify(studentRepository, never()).save(any());
    }

}
