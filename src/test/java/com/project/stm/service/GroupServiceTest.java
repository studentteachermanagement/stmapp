package com.project.stm.service;

import com.project.stm.model.Group;
import com.project.stm.repository.GroupRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GroupServiceTest {

    @Mock
    private GroupRepository groupRepository;

    @InjectMocks
    private GroupService groupService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllGroups() {
        List<Group> groups = new ArrayList<>();
        when(groupRepository.findAll()).thenReturn(groups);

        List<Group> result = groupService.getAllGroups();

        assertEquals(groups, result);
    }

    @Test
    void getGroupById() {
        Long groupId = 1L;
        Group group = new Group();
        when(groupRepository.findById(groupId)).thenReturn(Optional.of(group));

        Optional<Group> result = groupService.getGroupById(groupId);

        assertTrue(result.isPresent());
        assertEquals(group, result.get());
    }

    @Test
    void createGroup_ValidData_Success() {
        Group group = new Group();
        when(groupRepository.save(any(Group.class))).thenReturn(group);

        Group result = groupService.createGroup(group);

        assertEquals(group, result);
        verify(groupRepository, times(1)).save(group);
    }

    @Test
    void deleteGroup() {
        Long groupId = 1L;

        groupService.deleteGroup(groupId);

        verify(groupRepository, times(1)).deleteById(groupId);
    }

    @Test
    void getGroupCount() {
        long expectedCount = 5L;
        when(groupRepository.count()).thenReturn(expectedCount);

        Long result = groupService.getGroupCount();

        assertEquals(expectedCount, result);
        verify(groupRepository, times(1)).count();
    }
}
