package com.project.stm.service;

import com.project.stm.model.Course;
import com.project.stm.model.CourseType;
import com.project.stm.repository.CourseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CourseServiceTest {

    @Mock
    private CourseRepository courseRepository;

    @InjectMocks
    private CourseService courseService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllCourses() {
        List<Course> courses = new ArrayList<>();
        when(courseRepository.findAll()).thenReturn(courses);

        List<Course> result = courseService.getAllCourses();

        assertEquals(courses, result);
    }

    @Test
    void getCourseById_ExistingCourse_ReturnsCourse() {
        Long courseId = 1L;
        Course course = new Course();
        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));

        Course result = courseService.getCourseById(courseId);

        assertNotNull(result);
        assertEquals(course, result);
    }

    @Test
    void getCourseById_NonExistingCourse_ReturnsNull() {
        Long courseId = 1L;
        when(courseRepository.findById(courseId)).thenReturn(Optional.empty());

        Course result = courseService.getCourseById(courseId);

        assertNull(result);
    }

    @Test
    void createCourse_ValidData_Success() {
        Course course = new Course();
        when(courseRepository.save(any(Course.class))).thenReturn(course);

        Course result = courseService.createCourse(course);

        assertEquals(course, result);
        verify(courseRepository, times(1)).save(course);
    }

    @Test
    void deleteCourse() {
        Long courseId = 1L;

        courseService.deleteCourse(courseId);

        verify(courseRepository, times(1)).deleteById(courseId);
    }

    @Test
    void getNumberOfCoursesByType() {
        CourseType courseType = CourseType.MAIN;
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(1L, "Math101", CourseType.MAIN));
        courses.add(new Course(2L, "Physics101", CourseType.SECONDARY));

        when(courseRepository.findAll()).thenReturn(courses);

        long result = courseService.getNumberOfCoursesByType(courseType);

        assertEquals(1, result);
    }
}
