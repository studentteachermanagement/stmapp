package com.project.stm.service;

import com.project.stm.model.Teacher;
import com.project.stm.repository.TeacherRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TeacherServiceTest {

    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherService teacherService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllTeachers() {
        List<Teacher> teachers = new ArrayList<>();
        when(teacherRepository.findAll()).thenReturn(teachers);

        List<Teacher> result = teacherService.getAllTeachers();

        assertEquals(teachers, result);
    }

    @Test
    void getTeacherById() {
        Long teacherId = 1L;
        Teacher teacher = new Teacher();
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(teacher));

        Optional<Teacher> result = teacherService.getTeacherById(teacherId);

        assertTrue(result.isPresent());
        assertEquals(teacher, result.get());
    }

    @Test
    void createTeacher_InvalidData_ThrowsException() {
        Teacher invalidTeacher = new Teacher();
        assertThrows(IllegalArgumentException.class, () -> teacherService.createTeacher(invalidTeacher));
        verify(teacherRepository, never()).save(any(Teacher.class));
    }

    @Test
    void createTeacher_NullData_ThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> teacherService.createTeacher(null));
        verify(teacherRepository, never()).save(any(Teacher.class));
    }

    @Test
    void createTeacher_ValidData_SaveIsCalled() {
        Teacher validTeacher = new Teacher();
        validTeacher.setFirstName("John");
        validTeacher.setLastName("Doe");
        validTeacher.setAge(30L);

        when(teacherRepository.save(validTeacher)).thenReturn(validTeacher);

        Teacher result = teacherService.createTeacher(validTeacher);

        verify(teacherRepository, times(1)).save(validTeacher);
        assertEquals(validTeacher, result);
    }

    @Test
    void updateTeacher_InvalidData_ThrowsException() {
        Long teacherId = 1L;
        Teacher existingTeacher = new Teacher();
        Teacher invalidTeacher = new Teacher();
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(existingTeacher));

        assertThrows(IllegalArgumentException.class, () -> teacherService.updateTeacher(teacherId, invalidTeacher));
        verify(teacherRepository, never()).save(any(Teacher.class));
    }

    @Test
    void updateTeacher_ValidData_SaveIsCalled() {
        Long teacherId = 1L;
        Teacher existingTeacher = new Teacher();
        existingTeacher.setId(teacherId);

        Teacher validTeacher = new Teacher();
        validTeacher.setId(teacherId);
        validTeacher.setFirstName("Updated");
        validTeacher.setLastName("Name");
        validTeacher.setAge(25L);

        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(existingTeacher));
        when(teacherRepository.save(validTeacher)).thenReturn(validTeacher);

        Optional<Teacher> result = teacherService.updateTeacher(teacherId, validTeacher);

        verify(teacherRepository, times(1)).save(validTeacher);
        assertTrue(result.isPresent());
        assertEquals(validTeacher, result.get());
    }

    @Test
    void deleteTeacher() {
        Long teacherId = 1L;

        teacherService.deleteTeacher(teacherId);

        verify(teacherRepository, times(1)).deleteById(teacherId);
    }

    @Test
    void getTeacherCount() {
        long expectedCount = 5L;
        when(teacherRepository.count()).thenReturn(expectedCount);

        Long result = teacherService.getTeacherCount();

        assertEquals(expectedCount, result);
        verify(teacherRepository, times(1)).count();
    }

    @Test
    void patchTeacher_ValidData_SaveIsCalled() {
        Long teacherId = 1L;
        Teacher existingTeacher = new Teacher();
        existingTeacher.setId(teacherId);

        Map<String, Object> updates = new HashMap<>();
        updates.put("firstName", "Updated");
        updates.put("lastName", "Name");

        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(existingTeacher));
        when(teacherRepository.save(existingTeacher)).thenReturn(existingTeacher);

        Teacher result = teacherService.patchTeacher(teacherId, updates);

        verify(teacherRepository, times(1)).save(existingTeacher);
        assertEquals(existingTeacher, result);
    }

    @Test
    void patchTeacher_NonExistingTeacher_ThrowsException() {
        Long teacherId = 1L;
        Map<String, Object> updates = new HashMap<>();
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> teacherService.patchTeacher(teacherId, updates));
        verify(teacherRepository, never()).save(any(Teacher.class));
    }

    @Test
    void isInvalidTeacher_ValidTeacher_ReturnsFalse() {
        Teacher validTeacher = new Teacher();
        boolean result = teacherService.isInvalidTeacher(validTeacher);
        assertTrue(result);
    }

    @Test
    void isInvalidTeacher_NullTeacher_ReturnsTrue() {
        boolean result = teacherService.isInvalidTeacher(null);
        assertTrue(result);
    }

    @Test
    void isInvalidTeacher_MissingFirstName_ReturnsTrue() {
        Teacher teacher = new Teacher();
        teacher.setLastName("Last Name");
        boolean result = teacherService.isInvalidTeacher(teacher);
        assertTrue(result);
    }

    @Test
    void isInvalidTeacher_MissingLastName_ReturnsTrue() {
        Teacher teacher = new Teacher();
        teacher.setFirstName("First Name");
        boolean result = teacherService.isInvalidTeacher(teacher);
        assertTrue(result);
    }

    @Test
    void searchTeachers_NoCriteria_ReturnsAllTeachers() {
        List<Teacher> teachers = Arrays.asList(new Teacher(), new Teacher(), new Teacher());
        when(teacherRepository.findAll()).thenReturn(teachers);

        List<Teacher> result = teacherService.searchTeachers(null, null, null, null);

        assertEquals(teachers, result);
    }
}

