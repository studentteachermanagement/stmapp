package com.project.stm.controller;

import com.project.stm.model.Group;
import com.project.stm.service.GroupService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GroupControllerTest {

    @Mock
    private GroupService groupService;

    @InjectMocks
    private GroupController groupController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllGroups() {
        List<Group> groups = new ArrayList<>();
        when(groupService.getAllGroups()).thenReturn(groups);

        ResponseEntity<List<Group>> responseEntity = groupController.getAllGroups();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(groups, responseEntity.getBody());
    }

    @Test
    void getGroupById_ExistingGroup_ReturnsGroup() {
        Long groupId = 1L;
        Group group = new Group();
        when(groupService.getGroupById(groupId)).thenReturn(Optional.of(group));

        ResponseEntity<Group> responseEntity = groupController.getGroupById(groupId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(group, responseEntity.getBody());
    }

    @Test
    void getGroupById_NonExistingGroup_ReturnsNotFound() {
        Long groupId = 1L;
        when(groupService.getGroupById(groupId)).thenReturn(Optional.empty());

        ResponseEntity<Group> responseEntity = groupController.getGroupById(groupId);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNull(responseEntity.getBody());
    }

    @Test
    void createGroup_ValidData_ReturnsCreatedGroup() {
        Group groupToCreate = new Group();
        Group createdGroup = new Group();
        when(groupService.createGroup(groupToCreate)).thenReturn(createdGroup);

        ResponseEntity<Group> responseEntity = groupController.createGroup(groupToCreate);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(createdGroup, responseEntity.getBody());
    }

    @Test
    void deleteGroup_ExistingGroup_ReturnsOk() {
        Long groupId = 1L;
        when(groupService.getGroupById(groupId)).thenReturn(Optional.of(new Group()));

        ResponseEntity<Void> responseEntity = groupController.deleteGroup(groupId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        verify(groupService, times(1)).deleteGroup(groupId);
    }

    @Test
    void deleteGroup_NonExistingGroup_ReturnsNotFound() {
        Long groupId = 1L;
        when(groupService.getGroupById(groupId)).thenReturn(Optional.empty());

        ResponseEntity<Void> responseEntity = groupController.deleteGroup(groupId);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        verify(groupService, never()).deleteGroup(groupId);
    }
}
