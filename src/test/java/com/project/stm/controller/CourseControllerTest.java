package com.project.stm.controller;

import com.project.stm.model.Course;
import com.project.stm.model.CourseType;
import com.project.stm.service.CourseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CourseControllerTest {

    @Mock
    private CourseService courseService;

    @InjectMocks
    private CourseController courseController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllCourses() {
        List<Course> courses = new ArrayList<>();
        when(courseService.getAllCourses()).thenReturn(courses);

        ResponseEntity<List<Course>> responseEntity = courseController.getAllCourses();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(courses, responseEntity.getBody());
    }

    @Test
    void getCourseById_ExistingCourse_ReturnsCourse() {
        Long courseId = 1L;
        Course course = new Course();
        when(courseService.getCourseById(courseId)).thenReturn(course);

        ResponseEntity<Course> responseEntity = courseController.getCourseById(courseId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(course, responseEntity.getBody());
    }

    @Test
    void getCourseById_NonExistingCourse_ReturnsNotFound() {
        Long courseId = 1L;
        when(courseService.getCourseById(courseId)).thenReturn(null);

        ResponseEntity<Course> responseEntity = courseController.getCourseById(courseId);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNull(responseEntity.getBody());
    }

    @Test
    void createCourse_ValidData_ReturnsCreatedCourse() {
        Course courseToCreate = new Course();
        Course createdCourse = new Course();
        when(courseService.createCourse(courseToCreate)).thenReturn(createdCourse);

        ResponseEntity<Course> responseEntity = courseController.createCourse(courseToCreate);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(createdCourse, responseEntity.getBody());
    }

    @Test
    void deleteCourse() {
        Long courseId = 1L;

        ResponseEntity<Void> responseEntity = courseController.deleteCourse(courseId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        verify(courseService, times(1)).deleteCourse(courseId);
    }

    @Test
    void getNumberOfCoursesByType() {
        CourseType courseType = CourseType.MAIN;
        long count = 5L;
        when(courseService.getNumberOfCoursesByType(courseType)).thenReturn(count);

        ResponseEntity<Long> responseEntity = courseController.getNumberOfCoursesByType(courseType);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(count, responseEntity.getBody());
    }
}

