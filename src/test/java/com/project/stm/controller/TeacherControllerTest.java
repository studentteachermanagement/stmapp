package com.project.stm.controller;

import com.project.stm.model.Teacher;
import com.project.stm.service.TeacherService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
class TeacherControllerTest {

    @Mock
    private TeacherService teacherService;

    @InjectMocks
    private TeacherController teacherController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getTeacherById_ExistingTeacher_ReturnsTeacher() {
        Long teacherId = 1L;
        Teacher teacher = new Teacher();
        when(teacherService.getTeacherById(teacherId)).thenReturn(Optional.of(teacher));

        ResponseEntity<Teacher> response = teacherController.getTeacherById(teacherId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(teacher, response.getBody());
    }

    @Test
    void getTeacherById_NonExistingTeacher_ReturnsNotFound() {
        Long teacherId = 1L;
        when(teacherService.getTeacherById(teacherId)).thenReturn(Optional.empty());

        ResponseEntity<Teacher> response = teacherController.getTeacherById(teacherId);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void createTeacher_ValidData_Success() {
        Teacher teacher = new Teacher();
        when(teacherService.createTeacher(any(Teacher.class))).thenReturn(teacher);

        ResponseEntity<Teacher> response = teacherController.createTeacher(teacher);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(teacher, response.getBody());
    }

    @Test
    void updateTeacher_ExistingTeacher_ReturnsUpdatedTeacher() {
        Long teacherId = 1L;
        Teacher updatedTeacher = new Teacher();
        when(teacherService.updateTeacher(eq(teacherId), any(Teacher.class))).thenReturn(Optional.of(updatedTeacher));

        ResponseEntity<Teacher> response = teacherController.updateTeacher(teacherId, updatedTeacher);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedTeacher, response.getBody());
    }

    @Test
    void updateTeacher_NonExistingTeacher_ReturnsNotFound() {
        Long teacherId = 1L;
        Teacher updatedTeacher = new Teacher();
        when(teacherService.updateTeacher(eq(teacherId), any(Teacher.class))).thenReturn(Optional.empty());

        ResponseEntity<Teacher> response = teacherController.updateTeacher(teacherId, updatedTeacher);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void deleteTeacher_ExistingTeacher_ReturnsOk() {
        Long teacherId = 1L;
        when(teacherService.getTeacherById(teacherId)).thenReturn(Optional.of(new Teacher()));

        ResponseEntity<Void> response = teacherController.deleteTeacher(teacherId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(teacherService, times(1)).deleteTeacher(teacherId);
    }

    @Test
    void deleteTeacher_NonExistingTeacher_ReturnsNotFound() {
        Long teacherId = 1L;
        when(teacherService.getTeacherById(teacherId)).thenReturn(Optional.empty());

        ResponseEntity<Void> response = teacherController.deleteTeacher(teacherId);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(teacherService, never()).deleteTeacher(anyLong());
    }

    @Test
    void getTeacherCount() {
        when(teacherService.getTeacherCount()).thenReturn(5L);

        ResponseEntity<Long> response = teacherController.getTeacherCount();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(5L, response.getBody());
    }
}
