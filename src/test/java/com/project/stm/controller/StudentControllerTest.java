package com.project.stm.controller;

import com.project.stm.model.Student;
import com.project.stm.service.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class StudentControllerTest {

    @Mock
    private StudentService studentService;

    @InjectMocks
    private StudentController studentController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getStudentById_ExistingStudent_ReturnsStudent() {
        Long studentId = 1L;
        Student student = new Student();
        when(studentService.getStudentById(studentId)).thenReturn(Optional.of(student));

        ResponseEntity<Student> response = studentController.getStudentById(studentId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(student, response.getBody());
    }

    @Test
    void getStudentById_NonExistingStudent_ReturnsNotFound() {
        Long studentId = 1L;
        when(studentService.getStudentById(studentId)).thenReturn(Optional.empty());

        ResponseEntity<Student> response = studentController.getStudentById(studentId);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void createStudent_ValidData_Success() {
        Student student = new Student();
        when(studentService.createStudent(any(Student.class))).thenReturn(Optional.of(student));

        ResponseEntity<Student> response = studentController.createStudent(student);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(student, response.getBody());
    }

    @Test
    void createStudent_InvalidData_ReturnsBadRequest() {
        Student student = new Student();
        when(studentService.createStudent(any(Student.class))).thenReturn(Optional.empty());

        ResponseEntity<Student> response = studentController.createStudent(student);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void updateStudent_ExistingStudent_ReturnsUpdatedStudent() {
        Long studentId = 1L;
        Student updatedStudent = new Student();
        when(studentService.updateStudent(eq(studentId), any(Student.class))).thenReturn(Optional.of(updatedStudent));

        ResponseEntity<Student> response = studentController.updateStudent(studentId, updatedStudent);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedStudent, response.getBody());
    }

    @Test
    void updateStudent_NonExistingStudent_ReturnsNotFound() {
        Long studentId = 1L;
        Student updatedStudent = new Student();
        when(studentService.updateStudent(eq(studentId), any(Student.class))).thenReturn(Optional.empty());

        ResponseEntity<Student> response = studentController.updateStudent(studentId, updatedStudent);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void deleteStudent_ExistingStudent_ReturnsOk() {
        Long studentId = 1L;
        when(studentService.deleteStudent(studentId)).thenReturn(true);

        ResponseEntity<Void> response = studentController.deleteStudent(studentId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void deleteStudent_NonExistingStudent_ReturnsNotFound() {
        Long studentId = 1L;
        when(studentService.deleteStudent(studentId)).thenReturn(false);

        ResponseEntity<Void> response = studentController.deleteStudent(studentId);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void getStudentCount() {
        when(studentService.getStudentCount()).thenReturn(5L);

        ResponseEntity<Long> response = studentController.getStudentCount();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(5L, response.getBody());
    }
}

